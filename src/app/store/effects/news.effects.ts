import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

import { NewsService } from '../../services/news.service';

import { ESectionActionTypes,
  LoadSectionNews, LoadSectionNewsSuccess,
  SelectSection,
  FilterSubsection, FilterSubsectionSuccess } from '../actions/section.actions';

import { INewsItem } from '../../models/news-item.interface';


@Injectable()
export class NewsEffects {
  @Effect()
    getSectionNews$ = this.actions$.pipe(
      ofType<LoadSectionNews>(ESectionActionTypes.LOAD_SECTION_NEWS),
      map(action => action.payload),
      switchMap((section: string) => this.newsService.getSectionNews(section)),
      switchMap(res => {
        let results: INewsItem[];
        results = res["results"].map(i => {
//          console.log(i);
          return i;
        });
//        console.log(results);
        return of(new LoadSectionNewsSuccess(results),
          new FilterSubsection(results));
      })
    );

  @Effect()
    doFilterSubsection$ = this.actions$.pipe(
      ofType<FilterSubsection>(ESectionActionTypes.FILTER_SUBSECTION),
      map(action => action.payload),
      switchMap((news: INewsItem[]) => {
        let results: string[];
        results = news.map(i => {
          return i.subsection;
        });
        let rv = results.filter(
          function(item, pos){
            return item == '' ? false : results.indexOf(item) == pos;
          });
//        console.log("Eff: Filter works"+rv);
        return of(new FilterSubsectionSuccess(rv));
      })
    );

  constructor(private newsService: NewsService,
    private actions$: Actions) {}

}
