import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { SectionsService } from '../../services/sections.service';
import { LoadSections, ESectionActionTypes, LoadSectionsSuccess,
          SelectSection, LoadSectionNews } from '../actions/section.actions';

@Injectable()
export class AppEffects {
  @Effect()
  getSections$ = this.actions$.pipe(
    ofType<LoadSections>(ESectionActionTypes.LOAD_SECTIONS),
    switchMap(() => this.sectionsService.getSections()),
    switchMap((sections: string[]) => {
        console.log(sections);
        return of(new LoadSectionsSuccess(sections));
    })
  );

  constructor(private sectionsService: SectionsService,
    private actions$: Actions) {}
}
