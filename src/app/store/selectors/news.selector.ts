import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { INewsState } from '../state/news.state';
import { ISectionsState } from '../state/sections.state';

const newsState = (state: IAppState) => state.news;
const sectionsState = (state: IAppState) => state.sections;

//https://github.com/ngrx/platform/blob/master/docs/store/selectors.md
export const selectFilteredNews = createSelector(
  newsState,
  sectionsState,
  (nstate: INewsState, sstate: ISectionsState) => {
    let rv = nstate.news;

    if (rv != null)
    {
      rv = nstate.news.filter(
        function(item, pos){
          return item.subsection == sstate.filter;
      })
    }
    return rv;
  }
);
