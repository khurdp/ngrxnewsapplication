import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { INewsState } from '../state/news.state';
import { ISectionsState } from '../state/sections.state';

const sectionsState = (state: IAppState) => state.sections;

export const selectLoadSections = createSelector(
  sectionsState,
  (state: ISectionsState) => state.sections
);

export const selectSubsections = createSelector(
  sectionsState,
  (state: ISectionsState) => {
//    console.log(state.subsections);
    return state.subsections;
  }
);

export const selectSelectedSection = createSelector(
  sectionsState,
  (state: ISectionsState) => {
    return state.selectedSection;
  }
);
