import { RouterReducerState } from '@ngrx/router-store';

import { INewsState, initialNewsState } from './news.state';
import { ISectionsState, initialSectionsState } from './sections.state';

export interface IAppState {
  router?: RouterReducerState;
  news: INewsState;
  sections: ISectionsState;
}

export const initialAppState: IAppState = {
  news: initialNewsState,
  sections: initialSectionsState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
