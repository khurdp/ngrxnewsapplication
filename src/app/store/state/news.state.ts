import { INewsItem } from '../../models/news-item.interface';

export interface INewsState {
  news: INewsItem[];
  selectedNewsItem: INewsItem;
}

export const initialNewsState: INewsState = {
  news: null,
  selectedNewsItem: null,
};
