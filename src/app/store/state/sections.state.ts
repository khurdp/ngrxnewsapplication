export interface ISectionsState {
  selectedSection: string;
  filter: string;                 //selectedSubsection

  sections: string[];
  subsections: string[];
}

export const initialSectionsState: ISectionsState = {
  selectedSection: '',
  filter: '',

  sections: null,
  subsections: null
};
