import { sectionsReducer } from './sections.reducer';
import { initialSectionsState, ISectionsState } from '../state/sections.state';
import { ESectionActionTypes } from '../actions/section.actions';
import * as fromSectionsReducer from './sections.reducer';

describe('Sections Reducer', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = sectionsReducer(initialSectionsState, action);
      expect(result).toBe(initialSectionsState);
    });
    it('should return the list of sections, when LOAD_SECTIONS is dispatched', () => {
        // previous state + action = expectedState
        const state = [
            'home', 'opinion', 'world', 'national', 'politics', 'business', 'technology',
            'science', 'health', 'sports', 'arts', 'books', 'movies', 'theater', 'fashion',
            'food', 'travel', 'magazine', 'realestate', 'automobiles'
        ];
        const dummyState: ISectionsState = {
          selectedSection: '',
          filter: '',                 //selectedSubsection
          sections: state,
          subsections: []
        };

        const createAction = { type: ESectionActionTypes.LOAD_SECTIONS, payload: '' };
        const result = sectionsReducer(dummyState, createAction);

        // expectation
        expect(result.sections).toBe(state);
    });
    it('should return the initial state if no action is dispatched', () => {
        const state = [
            'home', 'opinion', 'world', 'national', 'politics', 'business', 'technology',
            'science', 'health', 'sports', 'arts', 'books', 'movies', 'theater', 'fashion',
            'food', 'travel', 'magazine', 'realestate', 'automobiles'
        ];
        const dummyState: ISectionsState = {
          selectedSection: '',
          filter: '',                 //selectedSubsection
          sections: state,
          subsections: []
        };

        const createAction = {type: '', payload: ''};
        const result = sectionsReducer(dummyState, createAction);

        expect(result.sections).toBe(state);
    });
    it('should return the filter value, when getFilter() is called', () => {
        const demoState = {
          sections: [ 'someData', 'Value' ],
          filter: 'movies'
        };
        const result = fromSectionsReducer.getFilter(demoState);
        console.log(result);

        expect(result).toBe('movies');
    });
});
