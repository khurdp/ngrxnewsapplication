import { Action } from '@ngrx/store';

import { ESectionActionTypes, SectionActions } from '../actions/section.actions';
import { initialNewsState, INewsState } from '../state/news.state';

//export const newsReducer =
export function newsReducer(state = initialNewsState, action: SectionActions): INewsState {
  switch (action.type) {
    case ESectionActionTypes.LOAD_SECTION_NEWS: {
      return state;
    }
    case ESectionActionTypes.LOAD_SECTION_NEWS_SUCCESS: {
//      console.log(action.payload);
      return {
        ...state,
        news: action.payload as any
      };
    }
    default:
      return state;
  }
}

export const getNewsList = (state: any) => {
    return state.news;
};
