import { newsReducer } from './news.reducer';
import { initialNewsState } from '../state/news.state';

describe('News Reducer', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = newsReducer(initialNewsState, action);

      expect(result).toBe(initialNewsState);
    });
});
