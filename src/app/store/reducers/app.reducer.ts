import { Action } from '@ngrx/store';
import { ActionReducerMap } from '@ngrx/store';

import { routerReducer } from '@ngrx/router-store';
import { IAppState } from '../state/app.state';
import { newsReducer } from './news.reducer';
import { sectionsReducer } from './sections.reducer';

export const appReducer: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  news: newsReducer,
  sections: sectionsReducer
};
