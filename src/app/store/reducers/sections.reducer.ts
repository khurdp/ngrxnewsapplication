import { Action } from '@ngrx/store';

import { ESectionActionTypes, SectionActions } from '../actions/section.actions';
import { initialNewsState, INewsState } from '../state/news.state';
import { initialSectionsState, ISectionsState } from '../state/sections.state';

export function sectionsReducer(state = initialSectionsState, action: SectionActions): ISectionsState {
  switch (action.type) {
    case ESectionActionTypes.UNSELECT_SUBSECTION: {
      return {
        ...state,
        filter: ''
      };
    }
    case ESectionActionTypes.SELECT_SUB_SECTION: {
      return {
        ...state,
        filter: action.payload as string
      };
    }
    case ESectionActionTypes.SELECT_SECTION: {
      return {
        ...state,
        selectedSection: action.payload as string
      };
    }
    case ESectionActionTypes.LOAD_SECTIONS: {
      return state;
    }
    case ESectionActionTypes.LOAD_SECTIONS_SUCCESS: {
//      console.log(action.payload);
      return {
        ...state,
        sections: action.payload as string[]
      };
    }
    case ESectionActionTypes.FILTER_SUBSECTION: {
      return state;
    }
    case ESectionActionTypes.FILTER_SUBSECTION_SUCCESS: {
//      console.log("Red: filter works:"+action.payload);
      return {
        ...state,
        subsections: action.payload as string[]
      };
    }
    default:
      return state;
  }
}

export const getFilter = (state: any) => {
    return state.filter;
};
