import { Action } from '@ngrx/store';
import { INewsItem } from '../../models/news-item.interface';

export enum ESectionActionTypes {
  LOAD_SECTIONS             = '[Section] Load Sections',
  LOAD_SECTIONS_SUCCESS     = '[Section] Load Sections Success',

  LOAD_SECTION_NEWS         = '[Section] Load Section News',
  LOAD_SECTION_NEWS_SUCCESS = '[Section] Load Section News Success',

  SELECT_SECTION            = '[Section] Select Section',
  SELECT_SUB_SECTION        = '[Section] Select Sub Section',
  UNSELECT_SUBSECTION       = '[Section] Unselect Sub Section',

  FILTER_SUBSECTION         = '[Section] Filter Subsection',
  FILTER_SUBSECTION_SUCCESS = '[Section] Filter Subsection Success'

}

export type SectionActions =
  SelectSection | SelectSubSection | UnselectSubsection |
  LoadSections | LoadSectionNews | FilterSubsection |
  LoadSectionsSuccess | LoadSectionNewsSuccess |
  FilterSubsectionSuccess;

export class UnselectSubsection implements Action {
  readonly type = ESectionActionTypes.UNSELECT_SUBSECTION;
  constructor(public payload: string) {}
}

export class SelectSubSection implements Action {
  readonly type = ESectionActionTypes.SELECT_SUB_SECTION;
  constructor(public payload: string) {}
}

export class SelectSection implements Action {
  readonly type = ESectionActionTypes.SELECT_SECTION;
  constructor(public payload: string) {}
}

export class LoadSections implements Action {
  readonly type: string = ESectionActionTypes.LOAD_SECTIONS;
  constructor(public payload: string) {}
}
export class LoadSectionsSuccess implements Action {
  readonly type = ESectionActionTypes.LOAD_SECTIONS_SUCCESS;
  constructor(public payload: string[]) {}
}

export class LoadSectionNews implements Action {
  readonly type = ESectionActionTypes.LOAD_SECTION_NEWS;
  constructor(public payload: string) {}
}
export class LoadSectionNewsSuccess implements Action {
  readonly type = ESectionActionTypes.LOAD_SECTION_NEWS_SUCCESS;
  constructor(public payload: INewsItem[]) {}
}

export class FilterSubsection implements Action {
  readonly type = ESectionActionTypes.FILTER_SUBSECTION;
  constructor(public payload: INewsItem[]) {}
}
export class FilterSubsectionSuccess implements Action {
  readonly type = ESectionActionTypes.FILTER_SUBSECTION_SUCCESS;
  constructor(public payload: string[]) {}
}
