import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from './store/state/app.state';
import { selectSelectedSection } from './store/selectors/app.selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'News Application';
  currentSection$ = this.store.pipe(select(selectSelectedSection));

  constructor(private store: Store<IAppState>){}

  ngOnInit(){
  }
}
