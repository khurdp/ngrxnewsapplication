import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from './components/news/news.component';

const routes: Routes = [
  { path: '', redirectTo: 'section/home', pathMatch: 'full' },
  { path: "section/:sectionName", component: NewsComponent },
  { path: "section/:sectionName/:subsection", component: NewsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
