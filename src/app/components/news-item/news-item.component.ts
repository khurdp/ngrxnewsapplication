import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from '../../store/state/app.state';
import { INewsItem } from '../../models/news-item.interface';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent implements OnInit {
  notAvailableImage = `${environment.assetsUrl}images/imageNotAvailableIcon.jpg`;
  display='none';
  @Input() cNewsItem: INewsItem = null;

  constructor(private store: Store<IAppState>) { }

  ngOnInit() {
    if (this.cNewsItem.multimedia.length === 0){
//      console.log(this.cNewsItem);
//      console.log(this.notAvailableImage);
    }
  }

  openModal(){
    this.display='block';
  }

  onCloseHandled(){
   this.display='none';
  }

  noop(){
    console.log('no op');
  }
}
