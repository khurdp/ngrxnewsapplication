import { StoreModule } from '@ngrx/store';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { NewsItemComponent } from './news-item.component';
import { INewsItem } from '../../models/news-item.interface';
import { appReducer } from '../../store/reducers/app.reducer';

describe('NewsItemComponent', () => {
  let component: NewsItemComponent;
  let fixture: ComponentFixture<NewsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(appReducer)
      ],
      declarations: [ NewsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsItemComponent);
    component = fixture.componentInstance;
    component.cNewsItem = {
      section: '',
      subsection: '',
      title: 'Let Us Catch You Up on the Biggest Stories in Politics This Week',
      abstract: '',
      url: '',
      byline: '',
      item_type: '',
      updated_date: '',
      created_date: '',
      published_date: '',
      material_type_facet: '',
      kicker: '',
      des_facet: [],
      org_facet: [],
      per_facet: [],
      geo_facet: [],
      multimedia: [{
        url: '',
        format: '',
        height: 0,
        width: 0,
        type: '',
        subtype: '',
        caption: '',
        copyright: '',
      }],
      short_url: ''
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display title in template', () => {
    let titleElement: HTMLElement = fixture.debugElement.query(By.css('h5')).nativeElement;

    expect(titleElement.innerText).toContain('Let Us Catch You Up on the Biggest Stories in Politics This Week');
  });

  it('should display news abstract in template', () => {
    let abstractElement: HTMLElement = fixture.debugElement.query(By.css('.media-body')).nativeElement;

    expect(abstractElement.innerText).toContain('Let Us Catch You Up on the Biggest Stories in Politics This Week\nRead more...');
  });
});
