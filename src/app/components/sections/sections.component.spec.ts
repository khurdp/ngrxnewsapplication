import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { StoreModule } from '@ngrx/store';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { SectionsComponent } from './sections.component';
import { appReducer } from '../../store/reducers/app.reducer';

describe('SectionsComponent', () => {
  let component: SectionsComponent;
  let fixture: ComponentFixture<SectionsComponent>;
  let sectionSubscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(appReducer),
        RouterTestingModule
      ],
      declarations: [ SectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should store the sections in array: sectionList ', async() => {
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.sections$).toBeDefined;
  });

  it ('should display the section name in DOM', () => {
    component.sections$ = Observable.create((observer: Observer<string[]>) => {
        setTimeout(() => {
          observer.next([
            'home', 'opinion', 'world', 'national', 'politics', 'business', 'technology',
            'science', 'health', 'sports', 'arts', 'books', 'movies', 'theater', 'fashion',
            'food', 'travel', 'magazine', 'realestate', 'automobiles'
          ]);
        }, 10);
    });

    sectionSubscription = component.sections$.subscribe(() => {
      let el: HTMLElement = fixture.debugElement.query(By.css('a')).nativeElement;

      fixture.detectChanges();
      expect(el.innerText).toContain('home');
      expect(el.innerText).toContain('Home');
    });
  });

  afterEach(() => {
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });

  afterAll(() => {
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });
});
