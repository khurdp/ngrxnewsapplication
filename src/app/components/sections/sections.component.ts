import { Component, OnInit, AfterContentInit, AfterViewChecked } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from '../../store/state/app.state';
import { LoadSections, LoadSectionNews,
        SelectSection, UnselectSubsection } from '../../store/actions/section.actions';
import { selectLoadSections, selectSelectedSection } from '../../store/selectors/app.selector';


@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})
export class SectionsComponent implements OnInit {
  sections$ = this.store.pipe(select(selectLoadSections));

  constructor(private store: Store<IAppState>) { }

  ngOnInit() {
    this.store.dispatch(new LoadSections(''));
    this.sectionClicked('home');
  }

  sectionClicked(sectionName: string){
//    console.log('section clicked - ' + sectionName);
    this.store.dispatch(new UnselectSubsection(''));
    this.store.dispatch(new SelectSection(sectionName));
    this.store.dispatch(new LoadSectionNews(sectionName));
  }
}

/*
This component should display different sections of news
    Subscribe and fetch the list of sections from store.
    Loop through the array and display each section name
    as routerLink.
    It should change the URL as /section/sectionName.
*/
