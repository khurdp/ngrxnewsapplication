import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from '../../store/state/app.state';
import { selectFilteredNews } from '../../store/selectors/news.selector';

import { INewsItem } from '../../models/news-item.interface';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  loading: boolean = false;
  newsItems$ = this.store.pipe(select(selectFilteredNews));

  constructor(private store: Store<IAppState>) { }

  ngOnInit() {
//    this.store.dispatch(new ShowFilteredNews()); //news that don't have a subsection
  }

}
