import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from '../../store/state/app.state';
import { FilterSubsection, SelectSubSection } from '../../store/actions/section.actions';
import { selectSubsections, selectSelectedSection } from '../../store/selectors/app.selector';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  subsections$ = this.store.pipe(select(selectSubsections));
  @Input() iCurrentSection: string = '';

  constructor(private store: Store<IAppState>) { }

  ngOnInit() {
//    console.log(this.iCurrentSection);
  }

  subsectionClicked(sectionName: string, subsection: string){
//    console.log('section clicked - ' + sectionName + ' ' + subsection);
    this.store.dispatch(new SelectSubSection(subsection));
  }
}

/*
Display Subsections
    Get the list of news from the store.
    Multiple news might be categorized in one subsection.
    Filter the list of unique subsections from the list
    and display in the navbar.

Filter News
    When a user clicks on subsection, it has to dispatch action
    and sends the subsection name as a parameter to the store.
    This action has to update the filter variable in the store.
*/
