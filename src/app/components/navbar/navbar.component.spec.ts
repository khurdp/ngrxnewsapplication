import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Store, StoreModule } from '@ngrx/store';
import { click } from '../../../../testing/index';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { NavbarComponent } from './navbar.component';
import { appReducer } from '../../store/reducers/app.reducer';
import { SelectSubSection } from '../../store/actions/section.actions';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let store;
  let subsectionSubscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(appReducer),
        RouterTestingModule
      ],
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define the store', () => {
    store = TestBed.get(Store);

    expect(store).toBeDefined;
  });

  it('should define subsections$', async() => {
    fixture.detectChanges();
    expect(component.subsections$).toBeDefined();
  });

  it('should display subsection in template', async() => {
    // assign mock values to subsections array
    component.subsections$ = Observable.create((observer: Observer<string[]>) => {
        setTimeout(() => {
          observer.next(['movie', 'tech', 'money']);
        }, 10);
    });
    subsectionSubscription = component.subsections$.subscribe( () => {
    // <a> tag in html will contain one of values of subsections array. select the <a> tag using query(By.css())
    const anchorElement = fixture.debugElement.query(By.css('a')).nativeElement.textContent;
    // after assigning values to subsections, detect changes. this is async call.
    fixture.detectChanges();

    expect(component.subsections$).toContain(anchorElement);
    });
  });

  it('should call subsectionClicked() when subsection is clicked', async() => {
    // you need to spy on the dispatchAction method
    spyOn(component, 'subsectionClicked');
    component.subsections$ = Observable.create((observer: Observer<string[]>) => {
        setTimeout(() => {
          observer.next(['movie', 'tech', 'money']);
        }, 10);
    });

    subsectionSubscription = component.subsections$.subscribe( () => {
      const anchorElement = fixture.debugElement.query(By.css('a'));
      click(anchorElement); //anchorElement.triggerEventHandler('click', null);

      // click contains asynchronous event handling. so you need to wait, for the event to process, by calling fixture.whenStable
      fixture.whenStable().then(() => {
        expect(component.subsectionClicked).toHaveBeenCalled();
      });
    });
  });

  it('should dispatch action when dispatchAction is called', async() => {
    // you need to spy on store's 'dispatch' method
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

    // if you call function dispatchAction with 'movies' paramter. expect store to dispatch action='movies'
    component.subsectionClicked('section', 'movies');
    fixture.detectChanges();

    expect(store.dispatch).toHaveBeenCalledWith(new SelectSubSection('movies'));
  });

  afterEach(() => {
    if (subsectionSubscription != null)
      subsectionSubscription.unsubscribe();
  });

  afterAll(() => {
    if (subsectionSubscription != null)
      subsectionSubscription.unsubscribe();
  });
});
