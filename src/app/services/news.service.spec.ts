import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { NewsService } from './news.service';
import { mockResponse } from './mockResponse';

describe('NewsService', () => {
  let service: NewsService;
  let httpMock: HttpTestingController;
  let sectionSubscription;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NewsService]
    })

  service = TestBed.get(NewsService);
  httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: NewsService = TestBed.get(NewsService);
    expect(service).toBeTruthy();
  });

  it('should get home section', () => {
//    const expectedUrl = 'https://api.nytimes.com/svc/topstories/v2/food.json?api-key=315a5a51483b469a918246dc2753b339';
    const expectedUrl = 'https://api.nytimes.com/svc/topstories/v2/home.json?api-key=315a5a51483b469a918246dc2753b339';

    sectionSubscription = service.getSectionNews('home').subscribe(received => {
            expect(received).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(expectedUrl);  // expectNone or match
    expect(req.request.method).toBe("GET");
    expect(req.request.url).toBe(expectedUrl);
    req.flush(mockResponse);
  });

  afterEach(() => {
    httpMock.verify();
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });

  afterAll(() => {
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });
});
