import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {
  assetsUrl = `${environment.assetsUrl}data/sections.json`;

  constructor(private http: HttpClient) { }

    getSections(): Observable<string[]> {
      return this.http.get<string[]>(this.assetsUrl);
    }
}
