import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SectionsService } from './sections.service';
import { environment } from '../../environments/environment';

describe('SectionsService', () => {
  let service: SectionsService;
  let httpMock: HttpTestingController;
  let sectionSubscription;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SectionsService]
    })
    service = TestBed.get(SectionsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: SectionsService = TestBed.get(SectionsService);
    expect(service).toBeTruthy();
  });
  it('should get all sections', () => {
    const allSections = [
    "arts", "automobiles", "books", "business", "fashion", "food", "health",
    "insider", "magazine", "movies", "national", "nyregion", "obituaries",
    "opinion", "politics", "realestate", "science", "sports", "sundayreview",
    "technology", "theater", "tmagazine", "travel", "upshot", "world"
    ];
    const assetsUrl = `${environment.assetsUrl}data/sections.json`;

    sectionSubscription = service.getSections().subscribe(received => {
            expect(received).toEqual(allSections);
    });

    const req = httpMock.expectOne(assetsUrl);  // expectNone or match
    expect(req.request.method).toBe("GET");
    expect(req.request.url).toBe(assetsUrl);
    req.flush(allSections);
  });

  afterEach(() => {
    httpMock.verify();
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });

  afterAll(() => {
    if (sectionSubscription != null)
      sectionSubscription.unsubscribe();
  });
});
