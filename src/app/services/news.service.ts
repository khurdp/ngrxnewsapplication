import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  apiRoot: string = "https://api.nytimes.com/svc/topstories/v2/";

  constructor(private http: HttpClient) { }

  getSectionNews(sectionName: string): Observable<any>{
    let apiURL = `${this.apiRoot}${sectionName}.json?api-key=315a5a51483b469a918246dc2753b339`;

    return this.http.get(apiURL);
  }
}
