# NgRxNewsApplication

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Commit log with additional info:

Try to fix the sections not showing issue
Fix the shortUrl issue.
Show section, subsection url. Add more tests.
Do some gui changes: Better image for 'Read more' and navbar.
b: home-run
Get ready to ship out:
b:morph-to-assignment
Finish as close to the course test requirements and specifications: split the state into news and sections and other needed changes.
Finish the assignment: Completed per requirement. Need to finish per specifications though.
Improvise and Add actions etc to show news item: just that. towards the larger goal.
Complete somewhat the navbar component: The navbar has been setup for router-outlet to show the subsections. Additions to store to support.
complete the sections and header components: just that.
get sections to load : Added INewsItem, INewsState, initialNewsState, IAppState, initialAppState, news and app reducer, sections service to load sections.json, effects and selectors. Got sections to load.
setup empty components and services: Passes ng test and loads landing page. Installed @ngrx/schematics too.
